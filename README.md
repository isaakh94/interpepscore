# README #

### Installation

Installing and using InterPepScore is fairly simple:

1. Make sure you have a functioning and up to date conda installation.

2. Apply for credentials for using PyRosetta, if you do not have them already.

3. Edit the channels `ipscore_environment.yml` file to your credentials for installing PyRosetta.

4. Run the following command: `conda env create -f ipscore_environment.yml`

### Using InterPepScore

In the `example.py` file you will find an example of how to run the FlexPepDock refinement protocol 
using InterPepScore as an additional scoreterm.
#!/usr/bin/env python

from pyrosetta import *
from pyrosetta.teaching import fa_atr, fa_rep, rama, PackRotamersMover, calc_Lrmsd
from pyrosetta.rosetta.protocols.docking import setup_foldtree
from pyrosetta.rosetta.protocols.simple_moves import SmallMover, ShearMover
from pyrosetta.rosetta.protocols.rigid import RigidBodyPerturbMover
from pyrosetta.rosetta.protocols.minimization_packing import RotamerTrialsMover, EnergyCutRotamerTrialsMover, RotamerTrialsMinMover, MinMover
from pyrosetta.rosetta.core.pack.task import TaskFactory, operation
from pyrosetta.rosetta.protocols.simple_task_operations import RestrictToInterface
from pyrosetta.rosetta.core.scoring.methods import EnergyMethod, SecondaryStructureEnergy, WholeStructureEnergy

import tensorflow as tf
import sonnet as snt
from graph_nets import utils_tf, modules

import numpy as np
import gzip
import pickle
import pickletools
import random
import time
from collections import deque
import itertools

sys.path.append(os.path.abspath("ipscore"))
from pepgraphnet import TestNet

class Memory():
    def __init__(self, max_size):
        self.buffer = deque(maxlen = max_size)
    def add(self, experience):
        self.buffer.append(experience)
    def buffersize(self):
        return len(self.buffer)
    def sample(self, batch_size):
        buffer_size = len(self.buffer)
        index = np.random.choice(np.arange(buffer_size), size = batch_size, replace = False)
        return [self.buffer[i] for i in index]
    def shuffle(self):
        max_size = self.buffer.maxlen
        self.buffer = list(self.buffer)
        random.shuffle(self.buffer)
        self.buffer = deque(self.buffer, maxlen = max_size)
    
class PepGuideScore():
    def __init__(self):
        #Initiate variables
        self.peptide_first_res = 0
        self.peptide_last_res = 0
        self.constant_edgepairs = None
        self.constant_edges = None
        self.nodes = None
        self.facit = None
        #The pre-learned matrix (currently BLOSUM62)
        self.aa_matrix = {'A': [4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, -1, -1], 'C': [0, -3, -3, -3, 9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -1, -3, -1], 'B': [-2, -1, 4, 4, -3, 0, 1, -1, 0, -3, -4, 0, -3, -3, -2, 0, -1, -4, -3, -3, 4, -3, 0, -1], 'E': [-1, 0, 0, 2, -4, 2, 5, -2, 0, -3, -3, 1, -2, -3, -1, 0, -1, -3, -2, -2, 1, -3, 4, -1], 'D': [-2, -2, 1, 6, -3, 0, 2, -1, -1, -3, -4, -1, -3, -3, -1, 0, -1, -4, -3, -3, 4, -3, 1, -1], 'G': [0, -2, 0, -1, -3, -2, -2, 6, -2, -4, -4, -2, -3, -3, -2, 0, -2, -2, -3, -3, -1, -4, -2, -1], 'F': [-2, -3, -3, -3, -2, -3, -3, -3, -1, 0, 0, -3, 0, 6, -4, -2, -2, 1, 3, -1, -3, 0, -3, -1], 'I': [-1, -3, -3, -3, -1, -3, -3, -4, -3, 4, 2, -3, 1, 0, -3, -2, -1, -3, -1, 3, -3, 3, -3, -1], 'H': [-2, 0, 1, -1, -3, 0, 0, -2, 8, -3, -3, -1, -2, -1, -2, -1, -2, -2, 2, -3, 0, -3, 0, -1], 'K': [-1, 2, 0, -1, -3, 1, 1, -2, -1, -3, -2, 5, -1, -3, -1, 0, -1, -3, -2, -2, 0, -3, 1, -1], 'J': [-1, -2, -3, -3, -1, -2, -3, -4, -3, 3, 3, -3, 2, 0, -3, -2, -1, -2, -1, 2, -3, 3, -3, -1], 'M': [-1, -1, -2, -3, -1, 0, -2, -3, -2, 1, 2, -1, 5, 0, -2, -1, -1, -1, -1, 1, -3, 2, -1, -1], 'L': [-1, -2, -3, -4, -1, -2, -3, -4, -3, 2, 4, -2, 2, 0, -3, -2, -1, -2, -1, 1, -4, 3, -3, -1], 'N': [-2, 0, 6, 1, -3, 0, 0, 0, 1, -3, -3, 0, -2, -3, -2, 1, 0, -4, -2, -3, 4, -3, 0, -1], 'Q': [-1, 1, 0, 0, -3, 5, 2, -2, 0, -3, -2, 1, 0, -3, -1, 0, -1, -2, -1, -2, 0, -2, 4, -1], 'P': [-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4, 7, -1, -1, -4, -3, -2, -2, -3, -1, -1], 'S': [1, -1, 1, 0, -1, 0, 0, 0, -1, -2, -2, 0, -1, -2, -1, 4, 1, -3, -2, -2, 0, -2, 0, -1], 'R': [-1, 5, 0, -2, -3, 1, 0, -2, 0, -3, -2, 2, -1, -3, -2, -1, -1, -3, -2, -3, -1, -2, 0, -1], 'T': [0, -1, 0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1, 1, 5, -2, -2, 0, -1, -1, -1, -1], 'W': [-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1, 1, -4, -3, -2, 11, 2, -3, -4, -2, -2, -1], 'V': [0, -3, -3, -3, -1, -2, -2, -3, -3, 3, 1, -2, 1, -1, -2, -2, 0, -3, -1, 4, -3, 2, -2, -1], 'Y': [-2, -2, -2, -3, -2, -1, -2, -3, 2, -1, -1, -2, -1, 3, -3, -2, -2, 2, 7, -1, -3, -1, -2, -1], 'X': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1], 'Z': [-1, 0, 0, 1, -3, 4, 4, -2, 0, -3, -3, 1, -1, -3, -1, 0, -1, -2, -2, -2, 0, -3, 4, -1]}
        for key in self.aa_matrix.keys():
            self.aa_matrix[key] = [float(i) for i in self.aa_matrix[key]]
        #Create network parameters and optimizers
        self.NN = TestNet()
        self.NN(utils_tf.data_dicts_to_graphs_tuple([{
            'globals': [1.0],
            'nodes': [[0.0 for j in range(25)] + self.aa_matrix['A'] for i in range(10)],
            'edges': [[1.0, 0.0, 0.0] for i in range(10)],
            'senders': [i for i in range(10)],
            'receivers': [i for i in range(10)]
        }]))#We need to run a dummy protein through the net to initialize weights
        self.optimizer = snt.optimizers.Adam(1e-3)
        self.batch_size = 16
        self.out = -10
        #Initialize Memory
        self.memory = Memory(10000)
        self.memorize_frequency = 9
        self.memory_counter = 0
        #Options
        self.lower_clip = 0.0
    
    def save(self, filename, save_memory = False, save_NN = True, save_optimizer = True):
        """
        Stores the NN with current weights, the Memory, and the optimizer.
        """
        if save_NN:
            pickle.dump(self.NN.variables, open(filename + '_NNvariables.p', 'wb'), protocol=4)
        if save_optimizer:
            pickle.dump(self.optimizer, open(filename + '_opt.p', 'wb'), protocol=4)
        if save_memory:
            with gzip.open(filename + '_mem.p.gz', 'wb') as f:
                pickled_memory = pickle.dumps(self.memory, protocol=4)
                optimized_pickled_memory = pickletools.optimize(pickled_memory)
                f.write(optimized_pickled_memory)
            
    def load(self, filename, load_memory = True, load_NN = True, load_optimizer = True):
        if os.path.isfile(filename + '_NNvariables.p') and load_NN:
            print('Loading network checkpoint')
            loaded_variables = pickle.load(open(filename + '_NNvariables.p', 'rb'))
            tf.group(*[var.assign(loadvar) for var, loadvar in zip(self.NN.variables, loaded_variables)])
        if os.path.isfile(filename + '_opt.p') and load_optimizer:
            print('Loading optimizer checkpoint')
            self.optimizer = pickle.load(open(filename + '_opt.p', 'rb'))
        if os.path.isfile(filename + '_mem.p.gz') and load_memory:
            print('Loading memory checkpoint')
            with gzip.open(filename + '_mem.p.gz', 'rb') as f:
                unpickler = pickle.Unpickler(f)
                self.memory = unpickler.load()
    
    def new_structure(self, pose):
        """
        Call this before using the Scorer to speed up performance
        """
        self.peptide_first_res = pose.chain_begin(2)
        self.peptide_last_res = pose.chain_end(2)
        self.constant_edgepairs, self.constant_edges = self.get_edges(pose, end_residue=self.peptide_first_res-1)
        #Creating the immutable node-list
        self.nodes = [self.aa_matrix[i] for i in pose.sequence()]
        self.nodes = [[0.0 for j in range(25)] + self.aa_matrix[i] for i in pose.sequence()]
        for i in range(self.peptide_first_res-1, self.peptide_last_res):
            self.nodes[i][i-self.peptide_first_res-1] = 1.0
        #Resetting facit
        self.facit = None
    
    def get_edges(self, pose, start_residue=1, end_residue=None):
        """
        Returns the edges of a pose.
        """
        graph = pose.energies().tenA_neighbor_graph()
        if end_residue == None:
            end_residue = self.peptide_last_res
        edgepairs = []
        edges = []
        for i in range(start_residue, end_residue+1):
            edgepairs.append([i-1, i-1])
            edges.append([1.0, 0.0, 0.0])
            #edges.append([1.0, 0.0])
        for i in range(1, end_residue):
            for j in range(max(i+1, start_residue), end_residue+1):
                if graph.get_edge_exists(i, j):
                    vector = [0.0, 0.0, 1.0]
                    if j-i == 1 and not i == self.peptide_first_res:
                        vector[1] = 1.0
                    edgepairs.append([i-1, j-1])
                    edges.append(vector)
                    #edges.append([0.0, 1.0])
                    edgepairs.append([j-1, i-1])
                    edges.append(vector)
                    #edges.append([0.0, 1.0])
        return edgepairs, edges
    
    def make_graph(self, pose):
        """
        Produces a graph readable by the NN.
        """
        new_edgepairs, new_edges = self.get_edges(pose, start_residue=self.peptide_first_res)
        edges = self.constant_edges + new_edges
        transformed_edges = list(zip(*(self.constant_edgepairs + new_edgepairs)))
        data_dict = {
            'globals': [1.0],
            'nodes': self.nodes,
            'edges': self.constant_edges + new_edges,
            'senders': transformed_edges[0],
            'receivers': transformed_edges[1]
        }
        return utils_tf.data_dicts_to_graphs_tuple([data_dict])
    
    def calc_loss(self, results, labels):
        return tf.stack([tf.compat.v1.losses.mean_squared_error(result[0], true) for result, true in zip(results, labels)])
    
    def train_step(self, samples, labels):
        with tf.GradientTape() as tape:
            results = self.NN(utils_tf.concat(samples, axis=0))
            loss = self.calc_loss(results, labels)
        gradients = tape.gradient(loss, self.NN.trainable_variables)
        self.optimizer.apply(gradients, self.NN.trainable_variables)
        return loss
    
    def train(self, nr_batches = 1000, batch_size = 16):
        """
        Call upon the current memory to train the net
        """
        total_losses = []
        train_length = min(nr_batches, self.memory.buffersize()//batch_size)
        total_losses = [0.0 for i in range(train_length)]
        for batchn in range(train_length):
            samples, labels = zip(*self.memory.sample(batch_size))
            losses = self.train_step(samples, labels)
            total_losses[batchn] = tf.reduce_mean(losses).numpy()
            print('[{:-<50}] {}/{} LOSS: {}'.format(''.join(['=' for i in range(int(50*(batchn/train_length)))]),
                                                    batchn*batch_size, 
                                                    train_length*batch_size, 
                                                    np.sum(total_losses)/((batchn+1))))
        return np.average(total_losses)
            
    def validate(self, batch_size = 16):
        """
        Runs through the entire memory in order, applying NN and calculating loss without back-propagation
        """
        valid_length = self.memory.buffersize()//batch_size
        total_losses = np.asarray([0.0 for i in range(valid_length*batch_size)])
        total_results = np.asarray([0.0 for i in range(valid_length*batch_size)])
        for batchn in range(valid_length):
            samples, labels = zip(*list(itertools.islice(self.memory.buffer,batchn*batch_size,(batchn+1)*batch_size)))
            results = self.NN(utils_tf.concat(samples, axis=0), is_training = False)
            losses = self.calc_loss(results, labels)
            total_losses[batchn*batch_size:(batchn+1)*batch_size] = losses.numpy()
            total_results[batchn*batch_size:(batchn+1)*batch_size] = np.asarray([result[0] for result in results])
            print('[{:-<50}] {}/{} VALLOSS: {}'.format(''.join(['=' for i in range(int(50*(batchn/valid_length)))]),
                                                       batchn*batch_size, 
                                                       valid_length*batch_size, 
                                                       np.sum(total_losses)/((batchn+1)*batch_size)))
        return total_losses, total_results
    
    def observe(self, pose):
        pose_graph = self.make_graph(pose)
        self.memory_counter += 1
        if self.memory_counter >= self.memorize_frequency and not self.facit == None:
            self.memory.add([pose_graph, 1/(1 + np.power(calc_Lrmsd(pose, self.facit, Vector1([1]))/4.0, 2))])
            self.memory_counter = 0
        return pose_graph
    
    def set_clip(self, lower, upper):
        self.lower_clip = lower
    
    def __call__(self, pose):
        pose_graph = self.observe(pose)
        self.out = self.NN(pose_graph, is_training = False)
        if not self.lower_clip == 0.0:
            self.out = np.clip(self.out - self.lower_clip, 0.0, 1.0)/(1.0-self.lower_clip)
        return self.out

global PEPGUIDESCORE_
PEPGUIDESCORE_ = PepGuideScore()
    
@pyrosetta.EnergyMethod()
class PepGuideScoreHelper(WholeStructureEnergy):
    """
    Scoring function calling a deep graph scorer
    REQUIRES A GLOBAL INSTANCE OF PepGuideScore PEPGUIDESCORE_ TO BE INITIALIZED
    Score is predicted as the S-normalized LRMSD of the ligand from facit site (norm around 4.0AA)
    Can be accessed from within a scorefxn by scorefxn.all_methods()[i]
    """
    def __init__(self):
        super().__init__(self.creator())
        # Initiate net
        self.NET = PEPGUIDESCORE_

    def finalize_total_energy(self, pose, _, emap):
        """
        THIS IS WHERE THE MAGIC HAPPENS
        This function is called once per pose
        """
        emap.set(self.scoreType, self.NET(pose))
        
@pyrosetta.EnergyMethod()
class PepGuideScoreObserver(WholeStructureEnergy):
    """
    Works just like PepGuideScoreHelper but only allows for observation, no scoring
    """
    def __init__(self):
        super().__init__(self.creator())
        self.NET = PEPGUIDESCORE_
    
    def finalize_total_energy(self, pose, _, emap):
        self.NET.observe(pose)

class FlexPepDockSimpleScorer(pyrosetta.rosetta.protocols.moves.Mover):
    """
    A copy of the FlexPepDock protocol utilizing a simple scoring-function
    found at main/source/src/protocols/flexpep_docking/FlexPepDockingProtocol.ccb 
    """
    def __init__(self, scorefxn, ramp_main = False):
        super().__init__()
        self.movemap = MoveMap()
        self.fpdscorefxn = get_fa_scorefxn()
        self.scorefxn = scorefxn
                
        self.lowres_abinitio = False
        self.lowres_preoptimize = False
        self.pep_refine = True

        self.ramp_main = ramp_main
        self.boost_atr = False
        self.ramp_rep = False
        self.ramp_rama = False

        self.rep_ramp_cycles = 10
        
        self.current_pose = Pose()
        self.native = Pose()

        self.rb_mover = RigidBodyPerturbMover(1, 7, 0.2)#jump, rot_mag, trans_mag
        self.small_mover = SmallMover(self.movemap, 0.8, 100)#temp, nmoves
        self.shear_mover = ShearMover(self.movemap, 0.8, 100)
        self.smove_angle_range = 6.0
        self.minimizer = None #will be created upon using apply
        self.use_minimizers = False

    def set_allowed_moves(self, pose, peptide_first_res=-1, peptide_last_res=-1):
        """
        Modifies the movemap to fit peptide operations
        """
        if peptide_first_res < 1 or peptide_last_res <= peptide_first_res:
            print('Inferring peptide residues from starting pose second chain')
            peptide_first_res = pose.chain_begin(2)
            peptide_last_res = pose.chain_end(2)
        print('\tPeptide first: {}, Peptide last: {}'.format(peptide_first_res, peptide_last_res))
        self.movemap.set_bb_true_range(peptide_first_res, peptide_last_res) #Backbone
        self.movemap.set_chi(True) #All sidechains
        self.movemap.set_jump(False) #Rigid-body operations
        self.movemap.set_jump(1, True)

    def rigidbody_monte_carlo_minimize(self, pose, packer, interface_tf_, allprotein_tf_, mcm_cycles=8):
        n_accepted = 0
        mc = MonteCarlo(pose, self.scorefxn, 0.8)
        #interface_rottrial = RotamerTrialsMover(self.scorefxn, interface_tf_)
        rottrial_ecut = EnergyCutRotamerTrialsMover(self.scorefxn, allprotein_tf_, mc, 0.05)
        #interface_rtmin = RotamerTrialsMinMover(self.scorefxn, interface_tf_)
        if self.use_minimizers:
            minimizer = MinMover(self.movemap, self.fpdscorefxn, "lbfgs_armijo_atol", 1.0, True)
        for i in range(1,mcm_cycles+1):
            self.rb_mover.apply(pose)
            if i % 8 == 0 or i == mcm_cycles - 1:
                packer.apply(pose)
            else:
                rottrial_ecut.apply(pose)
            if self.use_minimizers:
                current_score = self.scorefxn(pose)
                lowest_score = mc.lowest_score()
                if current_score - lowest_score < 15.0:
                    minimizer.apply(pose)
            if mc.boltzmann(pose):
                n_accepted += 1
        print('rigid-body-MCM -- {} out of {} monte carlo cycles accepted'.format(n_accepted, mcm_cycles))
        pose.assign(mc.lowest_score_pose())
        
    def torsions_monte_carlo_minimize(self, pose, packer, interface_tf_, allprotein_tf_, torsion_cycles=8):
        n_accepted = 0
        mc = MonteCarlo(pose, self.scorefxn, 0.8)
        rottrial_ecut = EnergyCutRotamerTrialsMover(self.scorefxn, allprotein_tf_, mc, 0.05)
        #interface_rtmin = RotamerTrialsMinMover(self.scorefxn, interface_tf_)
        if self.use_minimizers:
            minimizer = MinMover(self.movemap, self.fpdscorefxn, "lbfgs_armijo_atol", 1.0, True)
        for i in range(1,torsion_cycles+1):
            if i % 2 == 0:
                self.small_mover.apply(pose)
            else:
                self.shear_mover.apply(pose)
            if i % 8 == 0 or i == torsion_cycles - 1:
                packer.apply(pose)
            else:
                rottrial_ecut.apply(pose)
            if self.use_minimizers:
                current_score = self.scorefxn(pose)
                lowest_score = mc.lowest_score()
                if current_score - lowest_score < 15.0:
                    minimizer.apply(pose)
            if mc.boltzmann(pose):
                n_accepted += 1
        print('torsion-MCM -- {} out of {} monte carlo cycles accepted'.format(n_accepted, torsion_cycles))
        pose.assign(mc.lowest_score_pose())

    def hires_fpdock_protocol(self, pose):
        #Preparing for energy function ramping
        original_fpdscorefxn = self.fpdscorefxn.clone()
        original_scorefxn = self.scorefxn.clone()
        original_fa_atr = self.fpdscorefxn.get_weight(fa_atr)
        original_fa_rep = self.fpdscorefxn.get_weight(fa_rep)
        rep_ramp_step = (original_fa_rep - 0.02) / float(self.rep_ramp_cycles-1)
        original_rama = self.fpdscorefxn.get_weight(rama)
        rama_ramp_step = (original_rama - 0.01) / float(self.rep_ramp_cycles-1)
        #Constructing an interface packer and conducting the protocol
        task_pack_ = TaskFactory()
        task_pack_.push_back(operation.InitializeFromCommandline())
        task_pack_.push_back(operation.IncludeCurrent())
        task_pack_.push_back(operation.RestrictToRepacking())
        task_pack = task_pack_
        #task_pack = PackRotamersMover(self.fpdscorefxn)
        #task_pack.task_factory(task_pack_)
        task_pack_interface = TaskFactory()
        task_pack_interface.push_back(operation.InitializeFromCommandline())
        task_pack_interface.push_back(operation.IncludeCurrent())
        task_pack_interface.push_back(operation.RestrictToRepacking())
        task_pack_interface.push_back(RestrictToInterface(Vector1([1]))) #also known as interface_tf_
        interface_packer = PackRotamersMover(self.fpdscorefxn)
        interface_packer.task_factory(task_pack_interface)
        interface_packer.apply(pose)
        for i in range(1, self.rep_ramp_cycles + 1):
            if self.boost_atr:
                self.fpdscorefxn.set_weight(fa_atr, original_fa_atr*(1+0.025*(self.rep_ramp_cycles-i)))
                if self.ramp_main:
                    self.scorefxn.set_weight(fa_atr, original_fa_atr*(1+0.025*(self.rep_ramp_cycles-i)))
            if self.ramp_rep:
                self.fpdscorefxn.set_weight(fa_rep, 0.02*rep_ramp_step*float(i-1))
                if self.ramp_main:
                    self.scorefxn.set_weight(fa_rep, 0.02*rep_ramp_step*float(i-1))
            if self.ramp_rama:
                self.fpdscorefxn.set_weight(rama, 0.01*rama_ramp_step*float(i-1))
                if self.ramp_main:
                    self.fpdscorefxn.set_weight(rama, 0.01*rama_ramp_step*float(i-1))
            self.rigidbody_monte_carlo_minimize(pose, interface_packer, task_pack_interface, task_pack) 
            self.torsions_monte_carlo_minimize(pose, interface_packer, task_pack_interface, task_pack)
        if self.boost_atr or self.ramp_rep or self.ramp_rama:
            self.fpdscorefxn = original_fpdscorefxn.clone()
            if self.ramp_main:
                self.scorefxn = original_scorefxn.clone()
        self.scorefxn = original_scorefxn()
        self.minimizer.apply(pose)
        
    def check_filters(self, pose):
        #Optional score-filter (not implemented)
        return True
        
    def apply(self, pose, native = None):
        if not native:
            print('No native supplied, using copy of initial pose')
            self.native.assign(pose)
        setup_foldtree(pose, 'A_B', Vector1([1]))
        self.set_allowed_moves(pose)
        self.small_mover = SmallMover(self.movemap, 0.8, 100)#temp, nmoves
        self.small_mover.angle_max('L', self.smove_angle_range)
        self.shear_mover = ShearMover(self.movemap, 0.8, 100)
        self.shear_mover.angle_max('L', self.smove_angle_range)
        self.minimizer = MinMover(self.movemap, self.fpdscorefxn, "lbfgs_armijo_atol", 0.001, True)
        passed_filter = False
        self.current_pose.assign(pose)
        while not passed_filter:
            if self.lowres_abinitio:
                #Apply the FlexPepDockingAbinitio mover
                pass
            if self.lowres_preoptimize:
                #Apply the FlexPepDockingLowRes mover
                pass
            if self.pep_refine:
                self.hires_fpdock_protocol(pose)
            passed_filter = self.check_filters(pose)
            if not passed_filter:
                pose.assign(self.current_pose)

def main():
    """
    This main function is just here for easy testing of components
    """
    init()

    bscorefxn = get_fa_scorefxn()
    
    scorefxn = ScoreFunction()
    pepguide = PepGuideScoreHelper()
    scorefxn.set_weight(pepguide.scoreType, -25.0)
    
    observed_bscorefxn = get_fa_scorefxn()
    pepguide_observer = PepGuideScoreObserver()
    observed_bscorefxn.set_weight(pepguide_observer.scoreType, 1.0)
    mover = FlexPepDockSimpleScorer(scorefxn)
    mover.rep_ramp_cycles = 2
    mover_old = FlexPepDockSimpleScorer(observed_bscorefxn, ramp_main = True)
    mover_old.rep_ramp_cycles = 2
    
    PEPGUIDESCORE_.memorize_frequency = 1
    PEPGUIDESCORE_.numpy = True

    for i in range(3):
        pose = pose_from_pdb('test/test.pdb')
        pose2 = pose_from_pdb('test/test.pdb')
        bscorefxn(pose)
        PEPGUIDESCORE_.new_structure(pose)
        PEPGUIDESCORE_.facit = pose2
        start = time.time()
        if not i % 2 == 0:
            mover.apply(pose)
        else:
            mover_old.apply(pose)
        end = time.time()-start
        pose.dump_pdb('test/testtmp{}.pdb'.format(i))
        print(calc_Lrmsd(pose, pose2, Vector1([1])))
        print(end)
        if i == 2:
            print('VALIDATING NET')
            PEPGUIDESCORE_.validate()
            print(PEPGUIDESCORE_.memory.buffersize())
        print('TRAINING NET')
        PEPGUIDESCORE_.train()
        print(PEPGUIDESCORE_.memory.buffersize())
        if i == 0:
            PEPGUIDESCORE_.save('test/testtmp', save_memory=True)
        elif i == 1:
            PEPGUIDESCORE_.load('test/testtmp')
            
    print('If you got this far, you have managed to run FlexPepDock refinement with InterPepScore\nand trained it at that!')

if __name__ == '__main__':
    main()

#!/usr/bin/env python

import tensorflow as tf
import sonnet as snt
from graph_nets import utils_tf, modules
    
class TestNet(snt.Module):
    def __init__(self):
        super(TestNet, self).__init__()
        w_init = snt.initializers.TruncatedNormal(stddev=0.01)
        self.dropout_rate = 0.4
        self.graph_net1 = modules.GraphNetwork(
            edge_model_fn=lambda: snt.Linear(output_size = 8, w_init = w_init),
            node_model_fn=lambda: snt.Linear(output_size = 32, w_init = w_init),
            global_model_fn=lambda:snt.Linear(output_size = 8, w_init = w_init))
        self.bn1_edge = snt.BatchNorm(create_scale=True, create_offset=True)
        self.bn1_node = snt.BatchNorm(create_scale=True, create_offset=True)
        self.bn1_global = snt.BatchNorm(create_scale=True, create_offset=True)
        self.graph_net2 = modules.GraphNetwork(
            edge_model_fn=lambda: snt.Linear(output_size = 16, w_init = w_init),
            node_model_fn=lambda: snt.Linear(output_size = 16, w_init = w_init),
            global_model_fn=lambda:snt.Linear(output_size = 16, w_init = w_init))
        self.bn2_edge = snt.BatchNorm(create_scale=True, create_offset=True)
        self.bn2_node = snt.BatchNorm(create_scale=True, create_offset=True)
        self.bn2_global = snt.BatchNorm(create_scale=True, create_offset=True)
        self.graph_net3 = modules.GraphNetwork(
            edge_model_fn=lambda: snt.Linear(output_size = 16, w_init = w_init),
            node_model_fn=lambda: snt.Linear(output_size = 16, w_init = w_init),
            global_model_fn=lambda:snt.Linear(output_size = 16, w_init = w_init))
        self.bn3_global = snt.BatchNorm(create_scale=True, create_offset=True)
        self.lin1 = snt.Linear(64)
        self.lin2 = snt.Linear(1)
    def __call__(self, batch, is_training = True):
        if is_training:
            dropout = self.dropout_rate
        else:
            dropout = 0.0
        x = self.graph_net1(batch)
        x1 = x.replace(edges = tf.nn.dropout(tf.nn.elu(self.bn1_edge(x.edges, is_training = is_training)), rate = dropout),
                      nodes = tf.nn.dropout(tf.nn.elu(self.bn1_node(x.nodes, is_training = is_training)), rate = dropout),
                      globals = tf.nn.dropout(tf.nn.elu(self.bn1_global(x.globals, is_training = is_training)), rate = dropout))
        x = self.graph_net2(x1)
        x2 = x.replace(edges = tf.nn.dropout(tf.nn.elu(self.bn2_edge(x.edges, is_training = is_training)), rate = dropout),
                      nodes = tf.nn.dropout(tf.nn.elu(self.bn2_node(x.nodes, is_training = is_training)), rate = dropout),
                      globals = tf.nn.dropout(tf.nn.elu(self.bn2_global(x.globals, is_training = is_training)), rate = dropout))
        x = self.graph_net3(x2)
        x3 = tf.nn.dropout(tf.nn.elu(self.bn3_global(x.globals, is_training = is_training)), rate = dropout)
        x = tf.concat([x1.globals, x2.globals, x3], axis = 1)
        x = self.lin1(x)
        x = self.lin2(x)
        return x
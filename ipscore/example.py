#!/usr/bin/env python

import argparse

from pyrosetta import *
from pyrosetta.rosetta.protocols.flexpep_docking import FlexPepDockingFlags, FlexPepDockingPoseMetrics

import sys
sys.path.append(os.path.abspath("ipscore"))
from scorer import PepGuideScore, PEPGUIDESCORE_, PepGuideScoreHelper, PepGuideScoreObserver, FlexPepDockSimpleScorer
from scorer_lite import PepGuideScoreLite

def pept_length_to_score(x):
    return -31.875+(x*0.625)

list_of_score_features = ['fa_atr','fa_rep','fa_sol','fa_intra_rep','fa_intra_sol_xover4','lk_ball_w\
td','fa_elec','pro_close','hbond_sr_bb','hbond_lr_bb','hbond_bb_sc','hbond_sc','omega','fa_dun','p_a\
a_pp','yhh_planarity','ref','rama_prepro','total_score']                                             
extra_score_features = ['I_bsa', 'I_hb', 'I_pack', 'I_sc', 'I_unsat']

def give_unique_outfilename(outpdb, num=0):
    i = outpdb + '_' + str(num) + '.pdb'
    if os.path.isfile(i):
        return give_unique_outfilename(outpdb, num=num+1)
    return i

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pdb', type = str, help = 'Peptide must be chain B and all others must be chain A') 
    parser.add_argument('--peptide_length', type = int, default = 1)
    parser.add_argument('--checkpoint', type = str, default = 'memory/net')
    parser.add_argument('--output_prefix', type = str, default = 'test/testtmp')
    parser.add_argument('-n', '--nstruct', type = int, default = 1)
    parser.add_argument('--score', type = str, default = 'test/score.sc')
    args = parser.parse_args()
    
    init() #initializing PyRosetta
    
    #Loading the neural network and its weights
    scorefxn = get_fa_scorefxn()
    bscorefxn = get_fa_scorefxn()
    try:
        scoreweight = pept_length_to_score(args.peptide_length)
    except:
        scoreweight = -20.0
    #################################### YOU CAN USE EITHER THIS ##########################
    pepguide = PepGuideScoreLite()
    #####################################  OR THIS ########################################
    PEPGUIDESCORE_.load(args.checkpoint, load_memory = False, load_optimizer = False)
    pepguide = PepGuideScoreHelper()
    #######################################################################################
    # The differences are: PepGuideScoreHelper requires you to call PEPGUIDESCORE_.new_structure
    # every time a new complex should be considered, but is slightly faster and can be used
    # to retrain the scorer.
    scorefxn.set_weight(pepguide.scoreType, scoreweight)
    
    #Reading in the input structure
    originpose = pose_from_pdb(args.pdb)
    bscorefxn(originpose) #you need to score a pose at least once before calling new_structure
    PEPGUIDESCORE_.new_structure(originpose) ########## ONLY NECESSARY WITH PepGuideScoreHelper
    
    #These are just for some extra nice scoreterms in the output
    flags = FlexPepDockingFlags()
    flags.updateChains(originpose)
    extra_scores_scorer = FlexPepDockingPoseMetrics(flags)
    
    #Initiating scorefile
    with open(args.score, 'a') as f:
        f.write('SCORE,IPS,' + ','.join(list_of_score_features) + ',' + ','.join(extra_score_features) + ',pep_sc,name\n')
    #Running FlexPepDock refinement with the extra scoring term
    for i in range(args.nstruct):
        #Running FPD
        pose = Pose()
        pose.assign(originpose)
        FPD = FlexPepDockSimpleScorer(scorefxn, ramp_main = True)
        FPD.apply(pose)
        #Dumping PDB
        outpdb = '{}struct{}.pdb'.format(args.output_prefix,i)
        if os.path.isfile(outpdb):
            outpdb = give_unique_outfilename(outpdb[:-4])
        pose.dump_pdb(outpdb)
        #Writing to scorefile
        pept_energy = 0.0
        for resnum in range(flags.peptide_first_res(), flags.peptide_last_res() + 1):
            pept_energy = pose.energies().residue_total_energy(resnum)
        out_scores = pose.scores
        interface_scores = extra_scores_scorer.calc_interface_metrics(pose, 1, scorefxn)
        with open(args.score, 'a') as f:
            out = '{:6.4f},{:6.4f}'.format(out_scores['total_score'], out_scores['PyRosettaEnergy_first'])
            for scoreterm in list_of_score_features:
                out += ',{:6.4f}'.format(out_scores[scoreterm])
            for scoreterm in extra_score_features:
                out += ',{:6.4f}'.format(interface_scores[scoreterm])
            out += ',{:6.4f},{}\n'.format(pept_energy + out_scores['PyRosettaEnergy_first']*scoreweight, outpdb)
            f.write(out)
            
if __name__ == '__main__':
    main()
#!/usr/bin/env python

from pyrosetta import *
from pyrosetta.rosetta.core.scoring.methods import EnergyMethod, WholeStructureEnergy

import tensorflow as tf
from graph_nets import utils_tf

import argparse

@pyrosetta.EnergyMethod()
class PepGuideScoreLite(WholeStructureEnergy):
    """
    A scoring-function using pre-compiled graph-net for InterPepScore
    """
    def __init__(self):
        super().__init__(self.creator())
        self.aa_matrix = {'A': [4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, -1, -1], 'C': [0, -3, -3, -3, 9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -1, -3, -1], 'B': [-2, -1, 4, 4, -3, 0, 1, -1, 0, -3, -4, 0, -3, -3, -2, 0, -1, -4, -3, -3, 4, -3, 0, -1], 'E': [-1, 0, 0, 2, -4, 2, 5, -2, 0, -3, -3, 1, -2, -3, -1, 0, -1, -3, -2, -2, 1, -3, 4, -1], 'D': [-2, -2, 1, 6, -3, 0, 2, -1, -1, -3, -4, -1, -3, -3, -1, 0, -1, -4, -3, -3, 4, -3, 1, -1], 'G': [0, -2, 0, -1, -3, -2, -2, 6, -2, -4, -4, -2, -3, -3, -2, 0, -2, -2, -3, -3, -1, -4, -2, -1], 'F': [-2, -3, -3, -3, -2, -3, -3, -3, -1, 0, 0, -3, 0, 6, -4, -2, -2, 1, 3, -1, -3, 0, -3, -1], 'I': [-1, -3, -3, -3, -1, -3, -3, -4, -3, 4, 2, -3, 1, 0, -3, -2, -1, -3, -1, 3, -3, 3, -3, -1], 'H': [-2, 0, 1, -1, -3, 0, 0, -2, 8, -3, -3, -1, -2, -1, -2, -1, -2, -2, 2, -3, 0, -3, 0, -1], 'K': [-1, 2, 0, -1, -3, 1, 1, -2, -1, -3, -2, 5, -1, -3, -1, 0, -1, -3, -2, -2, 0, -3, 1, -1], 'J': [-1, -2, -3, -3, -1, -2, -3, -4, -3, 3, 3, -3, 2, 0, -3, -2, -1, -2, -1, 2, -3, 3, -3, -1], 'M': [-1, -1, -2, -3, -1, 0, -2, -3, -2, 1, 2, -1, 5, 0, -2, -1, -1, -1, -1, 1, -3, 2, -1, -1], 'L': [-1, -2, -3, -4, -1, -2, -3, -4, -3, 2, 4, -2, 2, 0, -3, -2, -1, -2, -1, 1, -4, 3, -3, -1], 'N': [-2, 0, 6, 1, -3, 0, 0, 0, 1, -3, -3, 0, -2, -3, -2, 1, 0, -4, -2, -3, 4, -3, 0, -1], 'Q': [-1, 1, 0, 0, -3, 5, 2, -2, 0, -3, -2, 1, 0, -3, -1, 0, -1, -2, -1, -2, 0, -2, 4, -1], 'P': [-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4, 7, -1, -1, -4, -3, -2, -2, -3, -1, -1], 'S': [1, -1, 1, 0, -1, 0, 0, 0, -1, -2, -2, 0, -1, -2, -1, 4, 1, -3, -2, -2, 0, -2, 0, -1], 'R': [-1, 5, 0, -2, -3, 1, 0, -2, 0, -3, -2, 2, -1, -3, -2, -1, -1, -3, -2, -3, -1, -2, 0, -1], 'T': [0, -1, 0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1, 1, 5, -2, -2, 0, -1, -1, -1, -1], 'W': [-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1, 1, -4, -3, -2, 11, 2, -3, -4, -2, -2, -1], 'V': [0, -3, -3, -3, -1, -2, -2, -3, -3, 3, 1, -2, 1, -1, -2, -2, 0, -3, -1, 4, -3, 2, -2, -1], 'Y': [-2, -2, -2, -3, -2, -1, -2, -3, 2, -1, -1, -2, -1, 3, -3, -2, -2, 2, 7, -1, -3, -1, -2, -1], 'X': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1], 'Z': [-1, 0, 0, 1, -3, 4, 4, -2, 0, -3, -3, 1, -1, -3, -1, 0, -1, -2, -2, -2, 0, -3, 4, -1]}
        self.NET = tf.saved_model.load('memory/graph')
        
    def get_nodes_(self, pose):
        """
        Returns the nodes of a pose
        """
        nodes = [[0.0 for j in range(25)] + self.aa_matrix[i] for i in pose.sequence()]
        for i in range(pose.chain_begin(2)-1, pose.chain_end(2)):
            nodes[i][i-pose.chain_begin(2)-1] = 1.0
        return nodes
        
    def get_edges_(self, pose):
        """
        Returns the edges of a pose.
        """
        end_residue = pose.chain_end(2)
        peptide_start = pose.chain_begin(2)
        graph = pose.energies().tenA_neighbor_graph()
        edgepairs = []
        edges = []
        for i in range(1, end_residue+1):
            edgepairs.append([i-1, i-1])
            edges.append([1.0, 0.0, 0.0])
        for i in range(1, end_residue):
            for j in range(i+1, end_residue+1):
                if graph.get_edge_exists(i, j):
                    vector = [0.0, 0.0, 1.0]
                    if j-i == 1 and not i == peptide_start:
                        vector[1] = 1.0
                    edgepairs.append([i-1, j-1])
                    edges.append(vector)
                    edgepairs.append([j-1, i-1])
                    edges.append(vector)
        return edgepairs, edges
        
    def make_graph_(self, pose):
        """
        Produces a graph readable by the NET.
        """
        edgepairs, edges = self.get_edges_(pose)
        transformed_edges = list(zip(*edgepairs))
        data_dict = {
            'globals': [1.0],
            'nodes': self.get_nodes_(pose),
            'edges': edges,
            'senders': transformed_edges[0],
            'receivers': transformed_edges[1]
        }
        return utils_tf.data_dicts_to_graphs_tuple([data_dict])
        
    def finalize_total_energy(self, pose, _, emap):
        emap.set(self.scoreType, self.NET.inference(self.make_graph_(pose)))
        
def test(checkpoint):
    from scorer import PepGuideScore, PEPGUIDESCORE_, PepGuideScoreHelper, PepGuideScoreObserver, FlexPepDockSimpleScorer
    #Some preparation
    init()
    scorefxn = ScoreFunction()
    scorefxnlite = ScoreFunction()
    bscorefxn = get_fa_scorefxn()
    #Initiating scores
    pepguidelite = PepGuideScoreLite()
    scorefxnlite.set_weight(pepguidelite.scoreType, -25.0)
    PEPGUIDESCORE_.load(checkpoint, load_memory = False, load_optimizer = False)
    pepguide = PepGuideScoreHelper()
    scorefxn.set_weight(pepguide.scoreType, -25.0)
    #Scoring poses
    pose = pose_from_pdb('test/test.pdb')
    bscorefxn(pose)
    PEPGUIDESCORE_.new_structure(pose)
    print('<===================================SCORE COMPARISON===================================>')
    print(scorefxn(pose))
    print(scorefxnlite(pose))
    print('<======================================================================================>')

def create_saved_graph(checkpoint, savepoint):
    import sys
    sys.path.append(os.path.abspath("ipscore"))
    from scorer import PepGuideScore, PEPGUIDESCORE_, PepGuideScoreHelper, PepGuideScoreObserver, FlexPepDockSimpleScorer
    import sonnet as snt
    from graph_nets import utils_tf
    
    init() #initializing PyRosetta
    
    #Loading the neural network and its weights
    PEPGUIDESCORE_.load(checkpoint, load_memory = False, load_optimizer = False)
    
    #Creating a tensorflow graph to save
    graph = utils_tf.data_dicts_to_graphs_tuple([{
            'globals': [1.0],
            'nodes': [[0.0 for j in range(25)] + [float(k) for k in [4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, -1, -1]] for i in range(10)],
            'edges': [[1.0, 0.0, 0.0] for i in range(10)],
            'senders': [i for i in range(10)],
            'receivers': [i for i in range(10)]
    }])
    @tf.function(input_signature = [utils_tf.specs_from_graphs_tuple(graph)])
    def inference(x):
        return PEPGUIDESCORE_.NN(x, is_training = False)
    #Only saving the necessary bits
    to_save = snt.Module()
    to_save.inference = inference
    to_save.all_variables = list(PEPGUIDESCORE_.NN.variables)
    #Save
    tf.saved_model.save(to_save, savepoint)
        
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--make_ipslite', action = 'store_true', help = 'Use a checkpoint-file to create a lite (untrainable) version of InterPepScore')
    parser.add_argument('--checkpoint', type = str, default = 'memory/net')
    parser.add_argument('--litecheckpoint', type = str, default = 'memory/graph')
    args = parser.parse_args()
    
    if args.make_ipslite:
        create_saved_graph(args.checkpoint, args.litecheckpoint)
    else:
        test(args.checkpoint)
    
if __name__ == '__main__':
    main()